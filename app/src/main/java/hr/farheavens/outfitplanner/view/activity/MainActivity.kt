package hr.farheavens.outfitplanner.view.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import hr.farheavens.outfitplanner.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
