package hr.farheavens.outfitplanner.view.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.bumptech.glide.Glide
import hr.farheavens.outfitplanner.R
import kotlinx.android.synthetic.main.loading_screen.*
import java.lang.Exception
import java.lang.Thread.sleep

class LoadingScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.loading_screen)

        Glide.with(this).load(R.drawable.loading_screen).into(img_loading_screen)

        val loadingScreenThread = Thread {

            try {
                sleep(10000)
            } catch (e: Exception) {
                print(e.message)
            } finally {
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            }

        }

        loadingScreenThread.start()

    }
}